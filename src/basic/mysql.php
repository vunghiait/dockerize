<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$servername = "mysql"; //links name in mysql service
$port = "3306"; //port default
//$username = "root";
//$password = "rootpass";
//$dbname = "dockerize_db";
$username = "db_username";
$password = "db_pass";
$dbname = "db_docker";

try {
    $conn = new PDO("mysql:host=$servername;port=$port;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected success";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
