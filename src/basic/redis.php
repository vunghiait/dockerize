<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require 'vendor/autoload.php';
require 'dump.php';

try {
    //Predis\Autoloader::register();
    $redis = new Redis();
    if (empty($_GET['method'])) {
        echo $redis->render();
        return true;
    }

    return call_user_func_array([$redis, $_GET['method']], $_GET);

} catch (Exception $e) {
    echo "Something wrong. " . $e->getMessage() . ' .File: ' . $e->getFile() . ' .Line: ' . $e->getLine() . ' .Code: ' . $e->getCode();
}

class Redis
{
    /** @var Predis\Client */
    protected $redis;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        $config = [
            "scheme" => "tcp",
            "host" => "redis",
            "port" => 6379,
            "password" => 'redis_password'
        ];
        $this->redis = new Predis\Client($config);
        return $this->redis;
    }

    public function health()
    {
        $ping = $this->redis->ping();
        $payload = $ping->getPayload();
        xz($payload, $ping);
    }

    public function info()
    {
        xxx($this->redis);
    }

    public function setGetKey()
    {
        $this->redis->del(['foo', 'key']);

        $this->redis->set('foo', 'bar');
        $value = $this->redis->get('foo');

        xx($value);
    }

    public function listRange()
    {
        $this->redis->lpush('key', ['A']);
        $this->redis->lpush('key', ['B', 'C']);
        $range = [
            'lrange' => $this->redis->lrange('key', 0, 500),
            'lpop' => $this->redis->lpop('key'),
            'lrange_2' => $this->redis->lrange('key', 0, 500),
        ];

        xx($range);
    }

    public function pipeline_TODO()
    {

    }

    public function pubSub_TODO()
    {

    }

    public function render()
    {
        $class = new ReflectionClass($this);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        $ignore = ['__construct', 'render'];

        $html = '<a target="_blank" href="https://github.com/phpredis/phpredis"> PHP Document</a><br/>';

        $html .= '<ol>';
        foreach ($methods as $method) {
            $name = $method->getName();
            if (in_array($name, $ignore)) {
                continue;
            }
            $html .= "<li><a href='/redis.php?method={$name}'>{$name}</a></li>";
        }
        $html .= '</ol>';

        return $html;
    }
}

