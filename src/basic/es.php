<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require 'vendor/autoload.php';
require 'dump.php';

try {
    $ES = new ES();
    if (empty($_GET['method'])) {
        echo $ES->render();
        return true;
    }

    return call_user_func_array([$ES, $_GET['method']], $_GET);

} catch (Exception $e) {
    echo "Something wrong. " . $e->getMessage() . ' .File: ' . $e->getFile() . ' .Line: ' . $e->getLine() . ' .Code: ' . $e->getCode();
}

class ES
{
    /** @var Elasticsearch\Client */
    protected $es;

    /** @var Faker\Generator */
    protected $faker;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        $host = 'elasticsearch'; //node.name
        $port = '9200';
        $config = ["{$host}:{$port}"];
        $this->es = Elasticsearch\ClientBuilder::create()->setHosts($config)->build();
        return $this->es;
    }

    public function health()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "elasticsearch");

        curl_setopt($ch, CURLOPT_PORT, "9200");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_exec($ch);

        $res = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        xx($res);
    }

    private $index = 'loggers_v1';
    //private $type = 'loggers_search';

    private $mappings = [
        'field_text' => [
            'type' => 'text',
            'analyzer' => 'reuters'
        ],
        'field_keyword' => [
            'type' => 'keyword'
        ],
        'field_array' => [
            'type' => 'integer'
        ],
        "field_date" => [
            "type" => "date",
            "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
        ],
        "field_boolean" => [
            "type" => "boolean"
        ],
        "field_ip" => [
            "type" => "ip"
        ],
        "field_integer" => [
            "type" => "integer"
        ],
        "field_float" => [
            "type" => "scaled_float",
            "scaling_factor" => 100
        ],
        "field_version" => [
            "type" => "version"
        ],
        "field_object" => [
            "properties" => [
                "field_object_integer" => [
                    "type" => "integer"
                ],
                "field_object_string" => [
                    "type" => "text"
                ],
            ]
        ],
        "field_nested" => [
            "type" => "nested",
            "properties" => [
                "field_nested_integer" => [
                    "type" => "integer"
                ],
                "field_nested_string" => [
                    "type" => "text"
                ],
            ]
        ],
        "field_created_at" => [
            "type" => "date",
            "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
        ],
        'field_re_index' => [
            'type' => 'integer'
        ],
    ];

    private function mappingFaker()
    {
        $this->faker = Faker\Factory::create();

        $data = [];
        foreach ($this->mappings as $field => $mapping) {
            if (!isset($mapping['properties'])) {
                $data[$field] = $this->dataFaker($field, $mapping);
                continue;
            }

            //nested
            if (isset($mapping['type']) && $mapping['type'] == 'nested') {
                $rand = rand(1, 3);

                $value = [];
                for ($i = 0; $i <= $rand; $i++) {
                    foreach ($mapping['properties'] as $property_field => $property_mapping) {
                        $value[$i][$property_field] = $this->dataFaker($property_field, $property_mapping);
                    }
                }
                $data[$field] = $value;

                continue;
            }

            //object
            foreach ($mapping['properties'] as $property_field => $property_mapping) {
                $value[$property_field] = $this->dataFaker($property_field, $property_mapping);
            }
            $data[$field] = $value;
        }
        return $data;
    }

    private function dataFaker($field, $mapping)
    {
        switch ($mapping['type']) {
            case 'text':
                $value = $this->faker->realText(rand(100, 200), $indexSize = 3);
                break;
            case 'keyword':
                $value = $this->faker->words(rand(3, 10), true);
                break;
            case 'array':
                $value = range(1, 5);
                break;
            case 'date':
                $value = $this->faker->date('Y-m-d H:i:s');
                break;
            case 'boolean':
                $value = rand(0, 1) == 1;
                break;
            case 'ip':
                $value = $this->faker->ipv4;
                break;
            case 'integer':
                $value = rand(1, 100);
                break;
            case 'scaled_float':
                $value = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100);
                break;
            case 'version':
                $value = rand(1, 2) . '.' . rand(1, 3) . '.' . rand(1, 3);
                break;
            default:
                $value = $this->faker->realText(rand(50, 100), $indexSize = 1);
                break;
        }

        switch ($field) {
            case 'field_array':
                $range = rand(1, 5);
                $value = range(1, $range);
                break;
            case 'field_created_at':
                $value = date('Y-m-d H:i:s');
                break;
        }

        return $value;
    }

    public function info()
    {
        xxx($this->es);
    }

    //create index
    public function createIndex()
    {
        if ($this->hasIndex()) {
            echo "index $this->index đã tồn tại";
            return true;
        }

        $settings = [
            'number_of_shards' => 3,
            'number_of_replicas' => 1,
            'analysis' => [
                'filter' => [
                    'shingle' => [
                        'type' => 'shingle'
                    ]
                ],
                'char_filter' => [
                    'pre_negs' => [
                        'type' => 'pattern_replace',
                        'pattern' => '(\\w+)\\s+((?i:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint))\\b',
                        'replacement' => '~$1 $2'
                    ],
                    'post_negs' => [
                        'type' => 'pattern_replace',
                        'pattern' => '\\b((?i:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint))\\s+(\\w+)',
                        'replacement' => '$1 ~$2'
                    ]
                ],
                'analyzer' => [
                    'reuters' => [
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => ['lowercase', 'stop', 'kstem']
                    ]
                ]
            ]
        ];

        $params = [
            'index' => $this->index,
            'body' => [
                'settings' => $settings,
                'mappings' => [
                    'properties' => $this->mappings
                ]
            ]
        ];
        $response = $this->es->indices()->create($params);

        echo 'Tạo index thành công';
        xx($response);
    }

    public function reIndex()
    {
        $params = ['index' => $this->index];
        $info = $this->es->indices()->getMapping($params);

        $mappings = $info[$this->index]['mappings'];

        $mappings['properties']['field_re_index'] = [
            'type' => 'text'
        ];

        $update = [
            'index' => $this->index,
            'body' => $mappings
        ];
        $response = $this->es->indices()->putMapping($update);
        echo 'Re index thành công';
        xx($response);
    }

    private function hasIndex()
    {
        return $this->es->indices()->exists([
            'index' => $this->index,
        ]);
    }

    //delete index
    public function deleteIndex()
    {
        if (!$this->hasIndex()) {
            echo "index $this->index không tồn tại";
            return true;
        }

        $params = [
            'index' => $this->index,
        ];
        $response = $this->es->indices()->delete($params);

        echo 'Xóa index thành công';
        xx($response);
    }

    public function settingIndex()
    {
        $params = ['index' => $this->index];
        $response = $this->es->indices()->getSettings($params);
        xx($response);
    }

    public function mappingIndex()
    {
        $params = ['index' => $this->index];
        $response = $this->es->indices()->getMapping($params);
        xx($response);
    }

    //curd
    public function listRecord()
    {
        $must = array();
        $should_must = array();
        $should = array();
        $must_not = array();

        $queries = [
            'index' => $this->index,
            'body' => [
                'from' => $_GET['page'] ?? 1,
                'size' => $_GET['per_page'] ?? 300,
                'query' => [
                    'bool' => [
                        'must' => $must,
                        'should' => $should,
                        'must_not' => $must_not,
                    ],
                ],
                'sort' => $_GET['sort'] ?? ['field_created_at' => 'desc'],
            ],
        ];

        $response = $this->es->search($queries);
        xz($response, $queries);
    }

    public function createRecord()
    {
        $params = [
            'index' => $this->index,
        ];
        $params['body'] = $this->mappingFaker();

        $response = $this->es->index($params);
        xz($response, $params);
    }

    public function create100Records()
    {
        set_time_limit(120);
        ini_set('memory_limit', '1024M');

        for ($i = 1; $i <= 100; $i++) {
            $params = [
                'index' => $this->index,
            ];
            $params['body'] = $this->mappingFaker();

            $response[] = $this->es->index($params);
        }

        xx('Create 100 records successfully');
    }

    private function getFirstId()
    {
        $queries = [
            'index' => $this->index,
            'body' => [
                'from' => 1,
                'size' => 1,
                'sort' => [
                    'post_date' => ['order' => 'desc']
                ]
            ],
        ];

        $response = $this->es->search($queries);

        $first = !empty($data = $response['hits']['hits'][0]) ? $data : [];
        return $first['_id'] ?? null;
    }

    public function showRecord()
    {
        $params = [
            'index' => $this->index,
            'id' => $_GET['id'] ?? $this->getFirstId()
        ];
        $response = $this->es->get($params);
        xz($response, $params);
    }

    public function updateRecord()
    {
        $params = [
            'index' => $this->index,
            'id' => $_GET['id'] ?? $this->getFirstId(),
            'body' => [
                'doc' => [
                    'title' => 'Title updated at ' . date('Y-m-d H:i:s') . '-' . time()
                ]
            ]
        ];

        $response = $this->es->update($params);

        unset($params['body']);
        $detail = $this->es->get($params);

        xz($params, $response, $detail);
    }

    public function deleteRecord()
    {
        $params = [
            'index' => $this->index,
            'id' => $_GET['id'] ?? $this->getFirstId()
        ];

        $response = $this->es->delete($params);
        xz($response, $params);
    }

    //boost -> tăng _score chính xác khi tìm kiếm
    public function boostSearch()
    {
        $must = array();
        $should_must = array();
        $should = array();
        $must_not = array();

        $q = $_GET['q'] ?? 'Alice';

        $should_must_string[] = [
            'match_phrase' => [
                'title' => [
                    'query' => trim($q),
                    'boost' => 3
                ]
            ],
        ];
        $should_must_string[] = [
            'match_phrase' => [
                'body' => [
                    'query' => strtolower($q),
                    'boost' => 0
                ]
            ],
        ];

        $must[] = ['bool' => ['minimum_should_match' => 1, 'should' => $should_must_string]];

        $queries = [
            'index' => $this->index,
            'body' => [
                'from' => $_GET['page'] ?? 1,
                'size' => $_GET['per_page'] ?? 300,
                'query' => [
                    'bool' => [
                        'must' => $must,
                        'should' => $should,
                        'must_not' => $must_not,
                    ],
                ],
                'sort' => ['_score' => 'desc'],
            ],
        ];

        $response = $this->es->search($queries);
        xz($response, $queries);
    }

    //script order -> sắp xếp theo nhu cầu
    public function scriptSort()
    {
        $must = array();
        $should_must = array();
        $should = array();
        $must_not = array();

        $sort = [
            "_script" => [
                "type" => "number",
                "order" => "desc",
                "script" => [
                    "lang" => "painless",
                    "source" => "doc['field_keyword'].value.length() * params.factor",
                    "params" => ["factor" => 1.1],
                ],

            ],
        ];

        $queries = [
            'index' => $this->index,
            'body' => [
                'from' => $_GET['page'] ?? 1,
                'size' => $_GET['per_page'] ?? 300,
                'query' => [
                    'bool' => [
                        'must' => $must,
                        'should' => $should,
                        'must_not' => $must_not,
                    ],
                ],
                'sort' => $sort,
            ],
        ];

        $response = $this->es->search($queries);
        xz($response, $queries);
    }

    //fuzzy search -> tìm kiếm sai chính tả
    public function fuzzySearch_TODO()
    {

    }

    //join search -> tìm kiếm theo quan hệ dữ liệu
    public function relationSearch_TODO()
    {

    }

    public function render()
    {
        $class = new ReflectionClass($this);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        $ignore = ['__construct', 'render'];

        $html = '<a href="chrome-extension://ffmkiejjmecolpfloofpjologoblkegm/elasticsearch-head/index.html" target="_blank">GUI: elasticsearch-head chrome extension</a> <br/>';
        $html .= '<a target="_blank" href="https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html"> Document</a><br/>';
        $html .= '<a target="_blank" href="https://www.elastic.co/guide/en/elasticsearch/client/php-api/current/index.html"> PHP Document</a><br/>';
        $html .= '<ol>';
        foreach ($methods as $method) {
            $name = $method->getName();
            if (in_array($name, $ignore)) {
                continue;
            }
            $html .= "<li><a href='/es.php?method={$name}'>{$name}</a></li>";
        }
        $html .= '</ol>';

        return $html;
    }
}